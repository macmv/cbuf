package cbuf

import (
  "testing"
)

func BenchmarkPush(b *testing.B) {
  c := New()

  for i := 0; i < b.N; i++ {
    c.Push([]byte{1})
    c.Pop(1)
  }
}
