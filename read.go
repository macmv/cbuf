package cbuf

import (
  "unsafe"
  "reflect"
)

// This reads as many bytes as possible into slice. Size is the number of bytes
// that were written into slice. Err will always be nil.
func (c *CyclicBuffer) Read(slice []byte) (size int, err error) {
  done := false
  index := 0
  amount := len(slice)
  c.mut.Lock()
  for !done {
    buffer := c.read.Value.([]byte)
    if c.read == c.write && c.read_index + amount >= c.write_index {
      // This means we will catch up to the writer, and will need
      // to return shorter data.
      read := copy(slice[index:], buffer[c.read_index:c.write_index])
      index += read
      amount -= read
      c.read_index += read
      done = true
    } else {
      // We either are on a different ring, or if we are on the same ring,
      // we will not catch up the the writer. In both cases, we just want to read.
      end := c.read_index + amount
      if end > c.buffer_size {
        end = c.buffer_size
      }
      read := copy(slice[index:], buffer[c.read_index:end])
      index += read
      amount -= read
      c.read_index += read
    }
    if amount <= 0 {
      done = true
    }
    if c.read_index >= c.buffer_size {
      c.read = c.read.Next()
      c.read_index -= c.buffer_size
    }
  }
  c.len -= index
  c.mut.Unlock()
  return index, nil
}

// This pulls out amount bytes from the buffer. This returns item in FIFO, hence the cyclic
// buffer. For example, if you Push([]byte{3, 4, 5}), then Pop(2) will return []byte{3, 4}.
func (c *CyclicBuffer) Pop(amount int) []byte {
  data := make([]byte, amount)
  size, _ := c.Read(data)
  return data[:size]
}

// This pops amount bytes, rounded down to the nearest border bytes.
func (c *CyclicBuffer) pop_even(amount, border int) []byte {
  if c.Len() % border != 0 {
    // Means we will get a slice that is not int aligned
    amount -= c.Len() - c.Len() % border
  }
  return c.Pop(amount)
}

// This pops amount ints. Depending on the size of the int type, this will
// pop a different number of bytes. Because of that, I only recommend using
// this buffer with PushInt(), and no other Push*() functions.
func (c *CyclicBuffer) PopInt(amount int) []int {
  size := int(unsafe.Sizeof(int(0)))
  data := c.pop_even(amount * size, size)
  // Now we cast data to []int
  header := *(*reflect.SliceHeader)(unsafe.Pointer(&data))
  header.Len /= size
  header.Cap /= size
  return *(*[]int)(unsafe.Pointer(&header))
}

// This pops amount int16s. This will always pop 2 bytes for each int.
func (c *CyclicBuffer) PopInt16(amount int) []int16 {
  size := 2
  data := c.pop_even(amount * size, size)
  // Now we cast data to []int16
  header := *(*reflect.SliceHeader)(unsafe.Pointer(&data))
  header.Len /= size
  header.Cap /= size
  return *(*[]int16)(unsafe.Pointer(&header))
}

// This pops amount int32s. This will always pop 4 bytes for each int.
func (c *CyclicBuffer) PopInt32(amount int) []int32 {
  size := 4
  data := c.pop_even(amount * size, size)
  // Now we cast data to []int32
  header := *(*reflect.SliceHeader)(unsafe.Pointer(&data))
  header.Len /= size
  header.Cap /= size
  return *(*[]int32)(unsafe.Pointer(&header))
}

// This pops amount int64s. This will always pop 8 bytes for each int.
func (c *CyclicBuffer) PopInt64(amount int) []int64 {
  size := 8
  data := c.pop_even(amount * size, size)
  // Now we cast data to []int64
  header := *(*reflect.SliceHeader)(unsafe.Pointer(&data))
  header.Len /= size
  header.Cap /= size
  return *(*[]int64)(unsafe.Pointer(&header))
}

// This pops amount float32s. This just reads 4 bytes amount times, and
// interprets then as floats. So if you write in an int32, and read a float32,
// you will get garbage data.
func (c *CyclicBuffer) PopFloat32(amount int) []float32 {
  size := 4
  data := c.pop_even(amount * size, size)
  // Now we cast data to []float32
  header := *(*reflect.SliceHeader)(unsafe.Pointer(&data))
  header.Len /= size
  header.Cap /= size
  return *(*[]float32)(unsafe.Pointer(&header))
}

// This pops amount float64s. This just reads 8 bytes amount times, and
// interprets then as floats. So if you write in an int64, and read a float64,
// you will get garbage data.
func (c *CyclicBuffer) PopFloat64(amount int) []float64 {
  size := 8
  data := c.pop_even(amount * size, size)
  // Now we cast data to []float64
  header := *(*reflect.SliceHeader)(unsafe.Pointer(&data))
  header.Len /= size
  header.Cap /= size
  return *(*[]float64)(unsafe.Pointer(&header))
}
