package cbuf

import (
  "sync"
  "strconv"
  "testing"

  "github.com/stretchr/testify/assert"
)

func TestWrite(t *testing.T) {
  c := NewSize(4)
  // Tests the most simple write
  c.Push([]byte{3, 4})

  expected_0 := make([]byte, 4)
  expected_0[0] = 3
  expected_0[1] = 4
  assert.Equal(t, expected_0, c.read.Value, "Writing data at the start should just call copy()")
  assert.Equal(t, c.read, c.write, "The write and read pointers should be the same")
  assert.Equal(t, 2, c.write_index, "The write index should have been incremented properly")

  // Tests another simple write, to make sure that write_index is working.
  c.Push([]byte{7})
  expected_0[2] = 7
  assert.Equal(t, expected_0, c.read.Value, "Writing data at the start should just call copy()")
  assert.Equal(t, c.read, c.write, "The write and read pointers should be the same")
  assert.Equal(t, 3, c.write_index, "The write index should have been incremented properly")

  // This should add another value to the ring list.
  c.Push([]byte{10, 12})
  expected_1 := make([]byte, 4)
  expected_0[3] = 10
  expected_1[0] = 12
  assert.Equal(t, expected_0, c.read.Value, "Writing data at the start should just call copy()")
  assert.Equal(t, expected_1, c.read.Next().Value, "Writing data over borders should make a new element")
  assert.NotEqual(t, c.read, c.write, "The write and read pointers should not be the same after grow")
  assert.Equal(t, 1, c.write_index, "The write index should have been incremented properly")

  // This test adding elements which will use multiple elements
  c.Push([]byte{1, 2, 3, 4, 5})
  expected_2 := make([]byte, 4)
  expected_1[1] = 1
  expected_1[2] = 2
  expected_1[3] = 3
  expected_2[0] = 4
  expected_2[1] = 5
  assert.Equal(t, expected_0, c.read.Value, "Writing data at the start should just call copy()")
  assert.Equal(t, expected_1, c.read.Next().Value, "Writing data over borders should make a new element")
  assert.Equal(t, expected_2, c.read.Next().Next().Value, "Writing data over borders should make a new element")
  assert.NotEqual(t, c.read, c.write, "The write and read pointers should not be the same after grow")
  assert.Equal(t, 2, c.write_index, "The write index should have been incremented properly")
  assert.Equal(t, 10, c.Len(), "After writing a lot of values, length should be 10")
}

func TestRead(t *testing.T) {
  c := NewSize(4)
  // Create a buffer with a lot of random data
  c.Push([]byte{1, 2, 3, 4, 5, 6, 7, 8, 9})

  assert.Equal(t, []byte{1}, c.Pop(1), "Reading one value should just get the oldest value")
  assert.Equal(t, 1, c.read_index, "The read index should have been incremented properly")
  assert.Equal(t, []byte{2}, c.Pop(1), "Reading another value should get the next value")
  assert.Equal(t, 2, c.read_index, "The read index should have been incremented properly")
  assert.Equal(t, []byte{3, 4, 5, 6, 7}, c.Pop(5), "Reading more values should work over multiple rings")
  assert.Equal(t, 3, c.read_index, "The read index should have been incremented properly")
  assert.Equal(t, []byte{8, 9}, c.Pop(4), "Reading too many values should return a short array")
  assert.Equal(t, 1, c.read_index, "The read index should have been incremented properly")
  assert.Equal(t, 0, c.Len(), "After reading too many values, length should be 0")

  c.Push([]byte{11})
  assert.Equal(t, []byte{11}, c.Pop(1), "Reading a value after a write should get the correct value")
  assert.Equal(t, 2, c.read_index, "The read index should have been incremented properly")
  assert.Equal(t, 0, c.Len(), "Length should be 0")
}

func TestWriteCircular(t *testing.T) {
  c := NewSize(4)
  for i := 0; i < 100; i++ {
    c.Push([]byte{1})
    c.Pop(1)
  }
  // The length will be 2, as on the first call to write, it ensures that it is not writing to the
  // same element as it is reading from. This is an unfortunate side effect, but is unavoidable.
  // The only way to avoid it would be to allow writes to the same element that it's reading from,
  // which opens up another can of worms that I couldn't really be bothered with.
  assert.Equal(t, c.read, c.write, "Read and write should be the same, when it's empty")
  assert.Equal(t, 2, c.write.Len(), "Write length should only be 2, after 100 writes")
  assert.Equal(t, 0, c.Len(), "Length should be 0 after a lot of pushes and pops")
}

func TestPopNumbers(t *testing.T) {
  if strconv.IntSize == 64 {
    t.Run("Int", func(t *testing.T) {
      c := New()
      c.Push([]byte{1, 2, 3, 4, 5, 6, 7, 8, 9, 10})
      data := c.PopInt(2)
      assert.Equal(t, 1, len(data), "Popping too many ints should return a shorter array")
      assert.Equal(t, 2, c.Len(), "Popping ints with an uneven border should result in the buffer not being empty")
    })
  }
  t.Run("Int16", func(t *testing.T) {
    c := New()
    c.Push([]byte{1, 2, 3})
    data := c.PopInt16(2)
    assert.Equal(t, 1, len(data), "Popping too many ints should return a shorter array")
    assert.Equal(t, 1, c.Len(), "Popping ints with an uneven border should result in the buffer not being empty")
  })
  t.Run("Int32", func(t *testing.T) {
    c := New()
    c.Push([]byte{1, 2, 3, 4, 5, 6})
    data := c.PopInt32(2)
    assert.Equal(t, 1, len(data), "Popping too many ints should return a shorter array")
    assert.Equal(t, 2, c.Len(), "Popping ints with an uneven border should result in the buffer not being empty")
  })
  t.Run("Int64", func(t *testing.T) {
    c := New()
    c.Push([]byte{1, 2, 3, 4, 5, 6, 7, 8, 9, 10})
    data := c.PopInt64(2)
    assert.Equal(t, 1, len(data), "Popping too many ints should return a shorter array")
    assert.Equal(t, 2, c.Len(), "Popping ints with an uneven border should result in the buffer not being empty")
  })
  t.Run("Float32", func(t *testing.T) {
    c := New()
    c.Push([]byte{1, 2, 3, 4, 5, 6})
    data := c.PopFloat32(2)
    assert.Equal(t, 1, len(data), "Popping too many ints should return a shorter array")
    assert.Equal(t, 2, c.Len(), "Popping ints with an uneven border should result in the buffer not being empty")
  })
  t.Run("Float64", func(t *testing.T) {
    c := New()
    c.Push([]byte{1, 2, 3, 4, 5, 6, 7, 8, 9, 10})
    data := c.PopFloat64(2)
    assert.Equal(t, 1, len(data), "Popping too many ints should return a shorter array")
    assert.Equal(t, 2, c.Len(), "Popping ints with an uneven border should result in the buffer not being empty")
  })
}

func TestPushNumbers(t *testing.T) {
  if strconv.IntSize == 64 {
    t.Run("Int", func(t *testing.T) {
      c := New()
      data := []int{1, 2}
      c.PushInt(data)
      assert.Equal(t, 16, c.Len(), "It should push 16 bytes after pushing 2 ints (this test only runs in 64 bit mode)")
      assert.Equal(t, data, c.PopInt(2), "Popping the data should return the same data that was pushed in")
    })
  }
  t.Run("Int16", func(t *testing.T) {
    c := New()
    data := []int16{1, 2}
    c.PushInt16(data)
    assert.Equal(t, 4, c.Len(), "It should push 4 bytes after pushing 2 int16s")
    assert.Equal(t, data, c.PopInt16(2), "Popping the data should return the same data that was pushed in")
  })
  t.Run("Int32", func(t *testing.T) {
    c := New()
    data := []int32{1, 2}
    c.PushInt32(data)
    assert.Equal(t, 8, c.Len(), "It should push 8 bytes after pushing 2 int32s")
    assert.Equal(t, data, c.PopInt32(2), "Popping the data should return the same data that was pushed in")
  })
  t.Run("Int64", func(t *testing.T) {
    c := New()
    data := []int64{1, 2}
    c.PushInt64(data)
    assert.Equal(t, 16, c.Len(), "It should push 16 bytes after pushing 2 int64s")
    assert.Equal(t, data, c.PopInt64(2), "Popping the data should return the same data that was pushed in")
  })
  t.Run("Float32", func(t *testing.T) {
    c := New()
    data := []float32{1, 2}
    c.PushFloat32(data)
    assert.Equal(t, 8, c.Len(), "It should push 8 bytes after pushing 2 float32s")
    assert.Equal(t, data, c.PopFloat32(2), "Popping the data should return the same data that was pushed in")
  })
  t.Run("Float64", func(t *testing.T) {
    c := New()
    data := []float64{1, 2}
    c.PushFloat64(data)
    assert.Equal(t, 16, c.Len(), "It should push 8 bytes after pushing 2 float64s")
    assert.Equal(t, data, c.PopFloat64(2), "Popping the data should return the same data that was pushed in")
  })
}

func TestMultithreadedWrite(t *testing.T) {
  TEST_SIZE := 8000
  var wg sync.WaitGroup
  wg.Add(2)
  // We want a lot of contention in the ring,
  // to acturately test multithreaded accuracy.
  c := NewSize(8)
  go func() {
    for i := 0; i < TEST_SIZE; i += 5 {
      c.Push([]byte{1, 2, 3, 4, 5})
    }
    wg.Done()
  }()
  go func() {
    for i := 0; i < TEST_SIZE; i += 5 {
      c.Push([]byte{1, 2, 3, 4, 5})
    }
    wg.Done()
  }()
  wg.Wait()
  assert.Equal(t, TEST_SIZE * 2, c.Len(), "Length should be correct after writing from multiple threads")
  expected := make([]byte, 0, TEST_SIZE * 2)
  for i := 0; i < TEST_SIZE * 2; i += 5 {
    expected = append(expected, 1, 2, 3, 4, 5)
  }
  assert.Equal(t, expected, c.Pop(TEST_SIZE * 2), "Data should be correect after writing from multiple threads")
}

func TestMultithreadedReadWrite(t *testing.T) {
  TEST_SIZE := 80000
  var wg sync.WaitGroup
  wg.Add(2)
  // We want a lot of contention in the ring,
  // to acturately test multithreaded accuracy.
  c := NewSize(8)
  for i := 0; i < TEST_SIZE * 2; i += 5 {
    c.Push([]byte{1, 2, 3, 4, 5})
  }
  go func() {
    for i := 0; i < TEST_SIZE; i += 5 {
      d := c.Pop(5)
      if len(d) != 5 {
        t.Fatal("Got the wrong number of bytes from c.Pop()")
      }
      if d[0] != 1 { t.Fatal("Got the wrong data from c.Pop()") }
      if d[1] != 2 { t.Fatal("Got the wrong data from c.Pop()") }
      if d[2] != 3 { t.Fatal("Got the wrong data from c.Pop()") }
      if d[3] != 4 { t.Fatal("Got the wrong data from c.Pop()") }
      if d[4] != 5 { t.Fatal("Got the wrong data from c.Pop()") }
    }
    wg.Done()
  }()
  go func() {
    for i := 0; i < TEST_SIZE; i += 5 {
      d := c.Pop(5)
      if len(d) != 5 {
        t.Fatal("Got the wrong number of bytes from c.Pop()")
      }
      if d[0] != 1 { t.Fatal("Got the wrong data from c.Pop()") }
      if d[1] != 2 { t.Fatal("Got the wrong data from c.Pop()") }
      if d[2] != 3 { t.Fatal("Got the wrong data from c.Pop()") }
      if d[3] != 4 { t.Fatal("Got the wrong data from c.Pop()") }
      if d[4] != 5 { t.Fatal("Got the wrong data from c.Pop()") }
    }
    wg.Done()
  }()
  wg.Wait()
  assert.Equal(t, 0, c.Len(), "Length should be zero after writing from multiple threads")
  assert.Equal(t, []byte{}, c.Pop(5), "Popping more should give an empty array")
}
