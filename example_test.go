package cbuf_test

import (
  "fmt"

  "gitlab.com/macmv/cbuf"
)

func ExampleCyclicBuffer_Pop() {
  c := cbuf.New()
  c.Push([]byte{3, 4, 5, 6, 7})

  // This returns the first 3 items added.
  fmt.Println(c.Pop(3))
  // There are only 2 items remaining, so this will return a shorter array.
  fmt.Println(c.Pop(3))
  // Output:
  // [3 4 5]
  // [6 7]
}
