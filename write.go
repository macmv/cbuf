package cbuf

import (
  "unsafe"
  "reflect"
  "container/ring"
)

// This writes data to the buffer. If the internal ring is full, this will grow the ring.
func (c *CyclicBuffer) Push(data []byte) {
  data_index := 0
  done := false
  c.mut.Lock()
  c.len += len(data)
  for !done {
    if c.write.Next() == c.read {
      // If this is true, we run into a problem: If c.read_index is large enough, it would probably
      // be fine to just start writing behind it in the same buffer. But, if we do that, then write_index
      // might catch up to read_index, and then we couldn't write anymore. So, if this is true, we
      // simply expand the ring, so that we are never writing to the same element that we are reading from.
      r := ring.New(1)
      r.Value = make([]byte, c.buffer_size)
      // Makes sure that we still reach c.read in the element after next
      r.Link(c.read)
      // Make the next element r
      c.write.Link(r)
    }
    buffer := c.write.Value.([]byte)
    written := copy(buffer[c.write_index:], data[data_index:])
    if written == len(data[data_index:]) {
      // If this is true, then we have written everything in data
      done = true
    }
    c.write_index += written
    data_index += written
    if c.write_index >= c.buffer_size {
      c.write = c.write.Next()
      c.write_index -= c.buffer_size
    }
  }
  c.mut.Unlock()
}

// This pushes data, but first casts it to a byte array. This is pushed in such a way
// that calling PopInt() will result in the same numbers that were pushed in. Since
// int can be variable length, it is recommended to only use this with PopInt().
func (c *CyclicBuffer) PushInt(data []int) {
  size := int(unsafe.Sizeof(int(0)))
  // Cast data to []byte
  header := *(*reflect.SliceHeader)(unsafe.Pointer(&data))
  header.Len *= size
  header.Cap *= size
  c.Push(*(*[]byte)(unsafe.Pointer(&header)))
}

// This pushes data, but first casts it to a byte array. This is pushed in such a way
// that calling PopInt16() will result in the same numbers that were pushed in.
func (c *CyclicBuffer) PushInt16(data []int16) {
  size := 2
  // Cast data to []byte
  header := *(*reflect.SliceHeader)(unsafe.Pointer(&data))
  header.Len *= size
  header.Cap *= size
  c.Push(*(*[]byte)(unsafe.Pointer(&header)))
}

// This pushes data, but first casts it to a byte array. This is pushed in such a way
// that calling PopInt32() will result in the same numbers that were pushed in.
func (c *CyclicBuffer) PushInt32(data []int32) {
  size := 4
  // Cast data to []byte
  header := *(*reflect.SliceHeader)(unsafe.Pointer(&data))
  header.Len *= size
  header.Cap *= size
  c.Push(*(*[]byte)(unsafe.Pointer(&header)))
}

// This pushes data, but first casts it to a byte array. This is pushed in such a way
// that calling PopInt64() will result in the same numbers that were pushed in.
func (c *CyclicBuffer) PushInt64(data []int64) {
  size := 8
  // Cast data to []byte
  header := *(*reflect.SliceHeader)(unsafe.Pointer(&data))
  header.Len *= size
  header.Cap *= size
  c.Push(*(*[]byte)(unsafe.Pointer(&header)))
}

// This pushes data, but first casts it to a byte array. This is pushed in such a way
// that calling PopFloat32() will result in the same numbers that were pushed in.
func (c *CyclicBuffer) PushFloat32(data []float32) {
  size := 4
  // Cast data to []byte
  header := *(*reflect.SliceHeader)(unsafe.Pointer(&data))
  header.Len *= size
  header.Cap *= size
  c.Push(*(*[]byte)(unsafe.Pointer(&header)))
}

// This pushes data, but first casts it to a byte array. This is pushed in such a way
// that calling PopFloat64() will result in the same numbers that were pushed in.
func (c *CyclicBuffer) PushFloat64(data []float64) {
  size := 8
  // Cast data to []byte
  header := *(*reflect.SliceHeader)(unsafe.Pointer(&data))
  header.Len *= size
  header.Cap *= size
  c.Push(*(*[]byte)(unsafe.Pointer(&header)))
}
