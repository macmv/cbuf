package cbuf

import (
  "sync"
  "container/ring"
)

// This is a cyclic buffer. It allows you to append data to the end of the buffer,
// and pop data from the front. It reuses an internal ring of slices, and expands
// that ring every time you write too much. All of that is hidden from you,
// as it is all performed by the Push*() and Pop*() functions.
type CyclicBuffer struct {
  // These are both pointers to the same ring.
  read *ring.Ring
  write *ring.Ring

  // Lock for the while buffer. Using a seperate read/write mutex might be helpful,
  // but I don't think it's worthwhile. You would need to lock the write mutex every
  // time you Pop() (you're reading from c.write_index all the time), so it would
  // only end up slowing things down.
  mut *sync.Mutex

  // Index within the ring element that read and write point to.
  read_index int
  write_index int

  // Length of the buffer within each ring element.
  buffer_size int

  // Length of the buffer, in bytes.
  len int
}

// This creates an empty cyclic buffer. It starts with 1024 bytes allocated, and
// will grow in chunks of 1024 every times it needs to. This is an alias to NewSize(1024).
func New() *CyclicBuffer {
  return NewSize(1024)
}

// This creates an empty cyclic buffer. It starts with size bytes allocated,
// and will use chunks of that many bytes every time it needs to.
func NewSize(size int) *CyclicBuffer {
  r := ring.New(1)
  r.Value = make([]byte, size)
  return &CyclicBuffer{
    read: r,
    write: r,
    mut: &sync.Mutex{},
    read_index: 0,
    write_index: 0,
    buffer_size: size,
    len: 0,
  }
}

func (c *CyclicBuffer) Len() int {
  c.mut.Lock()
  l := c.len
  c.mut.Unlock()
  return l
}
